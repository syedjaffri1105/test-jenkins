import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.*;
import java.util.concurrent.TimeUnit;

public class TESTAPP {
    protected ExtentReports     report;
    public static ExtentTest    logger;
    protected WebDriver driver = DriverSingleton.setDriver();
    protected String testCaseStatus = "PASSED";
    protected String browser =System.getProperty("browser");
    protected String browser1 =System.getProperty("environment");

    @BeforeClass(alwaysRun = true)
    public void beforeClassSetup() throws Exception {
        System.out.println("beforeClass");
        System.out.println(browser+browser1);
      //  driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        // Set the deployment server

        report = ExtentFactory.getInstance(this);
        logger = report.startTest("Loading Home Page.");
        Assert.assertEquals("a","a");
        logger.log(LogStatus.PASS, "Landing successfully on HomePage");
    }


    @Test(priority = 1, description = "HELLO WORLD")
    public void TESTHELOWORLD() throws Exception
    {
        logger = report.startTest("HELLO WORLD.");
        Assert.assertEquals("HELLO WORLD","HELLO WORLD" );
        logger.log(LogStatus.PASS, "Landing successfully on HomePage");

    }
    @AfterMethod
    public void screenCap(ITestResult result)
    {
        try
        {
            System.out.println("AfterMethod");
            if (result.getStatus() == ITestResult.FAILURE)
            {
                testCaseStatus = "FAILED";
                System.out.println(System.getProperty("user.dir"));
                String screenshot_path = ExtentFactory.captureScreenshot(result.getTestClass().getName());
                String imagePath = logger.addScreenCapture(screenshot_path);
                System.out.println(System.getProperty("imagePath"));
                logger.log(LogStatus.FAIL, "Failed ", imagePath);
            }
            if (result.getStatus() == ITestResult.SUCCESS)
            {
                testCaseStatus = "PASS";
                System.out.println(System.getProperty("user.dir"));
                String screenshot_path = ExtentFactory.captureScreenshot(result.getTestClass().getName());
                String imagePath = logger.addScreenCapture(screenshot_path);
                System.out.println(System.getProperty("imagePath"));
                logger.log(LogStatus.PASS, "INFO ", imagePath);
            }
        }

        catch (Exception e)
        {
            e.getMessage();
        }
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {

        System.out.println("Tearing down test " + getClass().getName() + "....");

        report.endTest(logger);
        report.flush();


    }

    @AfterSuite(alwaysRun = true)
    public void closeBrowser()
    {
        driver.close();
    }
}






